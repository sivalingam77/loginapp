package firstapp.android.com.loginapp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class registration extends AppCompatActivity {

    Button btn;
    ImageButton back;
    EditText usr,psd,mail,phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);
       // getActionBar().setDisplayHomeAsUpEnabled(true);
        usr=(EditText) findViewById(R.id.name);
        psd=(EditText) findViewById(R.id.psd);
        mail=(EditText) findViewById(R.id.mail);
        phone=(EditText) findViewById(R.id.phone);
        btn=(Button) findViewById(R.id.button);
        back=(ImageButton) findViewById(R.id.back);
        btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(usr.getText().toString().equals(""))
                {
                    Toast.makeText(getApplicationContext(),"enter user name",Toast.LENGTH_LONG).show();
                }
                else if(psd.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"enter password",Toast.LENGTH_LONG).show();
                }
                else if(mail.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"enter email address",Toast.LENGTH_SHORT).show();
                }
                else if(!isEmailValid(mail.getText().toString())){
                    Toast.makeText(getApplicationContext(),"enter valid id",Toast.LENGTH_LONG).show();
                }
                else if(phone.getText().toString().equals("")){
                    Toast.makeText(getApplicationContext(),"enter phone number",Toast.LENGTH_LONG).show();
                }else if(phone.getText().toString().length() < 10){
                    Toast.makeText(getApplicationContext(),"enter valid phone number",Toast.LENGTH_LONG).show();

                }else {
                    {
                        Toast.makeText(getApplicationContext(), "Login Success", Toast.LENGTH_LONG).show();
                        Intent myintent = new Intent(registration.this, Sucessfullogin.class);
                        startActivity(myintent);
                    }
                }

            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });

    }
    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;

    }
}
