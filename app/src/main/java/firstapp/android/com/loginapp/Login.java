package firstapp.android.com.loginapp;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Login extends MainActivity {

    Button signin;
    ImageButton back;
    EditText usd, password;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        //getActionBar().setDisplayHomeAsUpEnabled(true);

        usd = (EditText) findViewById(R.id.usd);
        password = (EditText) findViewById(R.id.password);
        signin = (Button) findViewById(R.id.signin);
        back=(ImageButton) findViewById(R.id.back);
        signin.setOnClickListener(new View.OnClickListener() {
            //private View v;

            @Override
            public void onClick(View v) {
              //  this.v = v;
                if (usd.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "enter email", Toast.LENGTH_LONG).show();
                } else if (password.getText().toString().equals("")) {
                    Toast.makeText(getApplicationContext(), "enter password", Toast.LENGTH_LONG).show();
                } else if (!isEmailValid(usd.getText().toString())) {
                    Toast.makeText(getApplicationContext(), "enter valid id", Toast.LENGTH_LONG).show();
                } else {

                    if (usd.getText().toString().equals("siva@gmail.com") && password.getText().toString().equals("password")) {
                        Toast.makeText(getApplicationContext(), "Login Success", Toast.LENGTH_LONG).show();
                        Intent myintent = new Intent(Login.this, Adminlogin.class);
                        startActivity(myintent);
                    }

                }
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }

    public static boolean isEmailValid(String email) {
        boolean isValid = false;

        String expression = "^[\\w\\.-]+@([\\w\\-]+\\.)+[A-Z]{2,4}$";
        CharSequence inputStr = email;

        Pattern pattern = Pattern.compile(expression, Pattern.CASE_INSENSITIVE);
        Matcher matcher = pattern.matcher(inputStr);
        if (matcher.matches()) {
            isValid = true;
        }
        return isValid;
    }
}