package firstapp.android.com.loginapp;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;

public class Adminlogin extends Login {

    ImageButton back;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adminlogin);
       // getActionBar().setDisplayHomeAsUpEnabled(true);
        back=(ImageButton) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
            }
        });
    }
}
